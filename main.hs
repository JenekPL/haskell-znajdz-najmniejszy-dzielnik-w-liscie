import Data.List

check :: Integer -> Int -> [Integer] -> Bool
check _ (-1) _ = True
check n i listArg = do
  let numberFromList = listArg !! i
  let result = mod numberFromList n
  if result == 0 then check n (i-1) listArg
  else False

getResult :: [Integer] -> Int -> Int
getResult tableArg i = do
    let number = tableArg !! i
    let result = check number ((length tableArg)-1) tableArg
    if result == True then i
    else if ((i+1) < (length tableArg)) then getResult tableArg (i+1)
    else -1

change :: Int -> [Integer] -> [Integer]
change n list = do
    let number1 = list !! n
    let number2 = list !! (n-1)
    let (first,second) = splitAt n list
    let secondnew = (tail second) 
    let firstnew = (init first)
    firstnew ++ [number1] ++ [number2] ++ secondnew

mySort :: [Integer] -> Int -> Int -> [Integer]
mySort notSorted 0 0 = notSorted
mySort notSorted 0 i = do mySort notSorted ((length notSorted)-1) (i-1)
mySort notSorted n i = do
    let number1 = notSorted !! n
    let number2 = notSorted !! (n-1)
    if number1 < number2 then 
        mySort (change n notSorted) (n-1) i
    else mySort notSorted (n-1) i

main = do
    kek <- getLine
    let list = (map read $ words kek :: [Integer])
    let sortedList = mySort list ((length list)-1) (length list)
    print sortedList
    let resultIndex = getResult sortedList 0
    if resultIndex == -1 then print "Nie ma liczby ktora byla by dzielnikiem wszystkich liczb na Liscie"
    else print (sortedList !! resultIndex)
